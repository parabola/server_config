. ${BUILDFILE%/*}/common.sh
pkgver=20161112

package() {
preamble
# #### process management

# Wherever possible, we should use uWSGI for process management of our
# HTTP services.  This allows for much more consistent management and
# configuration than the hodge-podge of PHP-FPM, manage.py, fcgiwrap,
# et c. that we used to have on Proton.

depends+=(uwsgi)

# uWSGI is the program, uwsgi is the protocol it speaks with nginx.  A
# pool of workers is called a vassal, and is configured in
# `/etc/uwsgi/${vassal}.ini`, and activated by
# `uwsgi@${vassal}.socket`; a socket speaking the uwsgi protocol is
# created at `/var/run/uwsgi/${vassal}.sock`.

# We use systemd socket activation rather than a uWSGI emperor because
# they provide largely the same functionality; the only real advantage
# that a uWSGI emperor would provide over systemd socket activation is
# if you ran it in tyrant mode, it lets you do some cool things with
# unpriveleged access, which would be useful for a shared web host.
# We aren't a shared web host, and have no reason to run emperor in
# tyrant mode.

# Since the `uwsgi@.service` vassal unit is written to support
# socket-activated or non-socket-activated use, it is normally
# possible to accidentally start it without the associated `.socket`
# unit; which is an error with how our vassal configurations are
# written.  To fix this, `uwsgi@.service.d/socket.conf` overrides the
# unit a bit to disable non-socket-activated use.
add-file etc/systemd/system/uwsgi@.service.d/socket.conf <<EOF
# Avoid accidentally starting the service without the socket
[Unit]
Requires=uwsgi@%i.socket
After=uwsgi@%i.socket
EOF

# The ownership and permissions for the socket are configured in
# `uwsgi@.socket.d/owner.conf`, which sets the owner to `http:http`
# and the mode to 0600.
add-file etc/systemd/system/uwsgi@.socket.d/owner.conf <<EOF
[Socket]
SocketUser=http
SocketGroup=http
SocketMode=0600
EOF

# uWSGI supports thread pools in addition to process pools, but many
# of the actual workers you'll want to use aren't thread safe, so
# stick to process pools unless you specifically know that your worker
# is thread-safe (for example, PHP, at least with the modules needed
# for MediaWiki, is not thread-safe).

# Individual vassal configurations are documented in the section for
# the service that they provide, not here.

postamble
}
