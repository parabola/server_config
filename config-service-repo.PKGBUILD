. ${BUILDFILE%/*}/common.sh
pkgver=20170204

provides=(pacman-mirrorlist)
conflicts=(pacman-mirrorlist)

package() {
preamble
# #### Repo

depends+=(
	rsync

	abs
	xbs
	mktorrent
	dbscripts

	config-mgmt-nginx
	uwsgi-plugin-php
	unionfs-fuse
	oxygen-icons
)

add-file etc/pacman.d/mirrorlist <<EOF
Server = file:///srv/repo/main/\$repo/os/\$arch
EOF

add-file -m755 etc/ssl/misc/certbot-get.d/10-repo <<EOF
#!/bin/bash
echo {repo,repomirror}.{parabola.nu,parabolagnulinux.org}
EOF

add-file -m644 etc/nginx/sites/server-repo_parabola_nu.conf <<EOF
# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-

server {
	server_name repo.parabola.nu;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	error_log  /var/log/nginx/nginx.http.repo_parabola_nu.error.log  error;
	access_log /var/log/nginx/nginx.http.repo_parabola_nu.access.log specific;

	if (\$args !~* noredirect) {
		return 302 https://repomirror.parabola.nu\$uri;
	}

	index /.fancyindex/index.php;
	location = /.fancyindex/index.php {
		include uwsgi_params;
		uwsgi_modifier1 14; # Standard PHP request
		uwsgi_pass unix:/run/uwsgi/repo.sock;
	}
	location / {
		root /srv/repo;
		try_files /main\$uri /http\$uri @union;
	}
	location @union {}
}
EOF

add-file -m644 etc/uwsgi/repo.ini <<EOF
[uwsgi]
master = true
processes = 4

uid = nobody
gid = http

plugins = php

php-set = open_basedir=/srv/http/repo.parabola.nu
EOF

add-unit etc/systemd/system/sockets.target.wants/uwsgi@repo.socket

add-file -m644 etc/nginx/sites/server-repomirror_parabola_nu.conf <<EOF
# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-

server {
	server_name repomirror.parabola.nu;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	error_log  /var/log/nginx/nginx.http.repomirror_parabola_nu.error.log  error;
	access_log /var/log/nginx/nginx.http.repomirror_parabola_nu.access.log specific;

	location / {
		root /srv/http/repo.parabola.nu;

		include uwsgi_params;
		uwsgi_param SCRIPT_NAME /srv/http/repo.parabola.nu/.fancyindex/mirror.php;
		uwsgi_modifier1 14; # Standard PHP request
		uwsgi_pass unix:/run/uwsgi/repomirror.sock;
	}
}
EOF

add-file -m644 etc/uwsgi/repomirror.ini <<EOF
[uwsgi]
master = true
processes = 4

uid = nobody
gid = http

plugins = php

php-app = /srv/http/repo.parabola.nu/.fancyindex/mirror.php
php-set = open_basedir=/srv/http/repo.parabola.nu:/srv/repo/http
EOF

add-unit etc/systemd/system/sockets.target.wants/uwsgi@repomirror.socket

add-file -m644 etc/systemd/system/rsyncd@.service.d/user.conf <<EOF
[Service]
User=nobody
EOF

add-file -m644 etc/systemd/system/rsyncd.socket.d/port.conf <<EOF
[Socket]
# 873 is filtered
ListenStream=
ListenStream=875
EOF

add-unit etc/systemd/system/sockets.target.wants/rsyncd.socket

add-file -m644 usr/share/holo/files/10-"$pkgname"/etc/rsyncd.conf <<EOF
# The uid, port, et c. are configured in the systemd units.

use chroot = no

[repos]
        path = /srv/repo/main
        comment = Parabola repos
        ignore nonreadable = yes

[abs]
        path = /srv/repo/db-import-archlinux-src/abslibre
        comment = Arch Build System Libre
EOF

postamble
}
