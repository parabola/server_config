. ${BUILDFILE%/*}/common.sh
pkgver=20170131

package() {
preamble
# #### Nginx

depends+=(nginx)

# `fastcgi.conf`, `fastcgi_params`, `scgi_params` and `uwsgi_params`
# have been edited to pass 127.0.0.1 as the client IP address to
# worker processes, to protect user privacy.
add-file -m755 usr/share/holo/files/10-"$pkgname"/etc/nginx/fastcgi.conf.holoscript <<EOF
#!/bin/sh
{
	echo '# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-'
	echo
	sed -e '/^# -\\*- /d' -e 's/\\\$remote_addr;\$/127.0.0.1; # \$remote_addr; # Anonymize/'
} | cat -s
EOF
ln -sfT fastcgi.conf.holoscript usr/share/holo/files/10-"$pkgname"/etc/nginx/fastcgi_params.holoscript
ln -sfT fastcgi.conf.holoscript usr/share/holo/files/10-"$pkgname"/etc/nginx/scgi_params.holoscript
ln -sfT fastcgi.conf.holoscript usr/share/holo/files/10-"$pkgname"/etc/nginx/uwsgi_params.holoscript

# `mime.types` has had xz, gzip, bzip2, tar, PGP (`.sig`), and
# bittorrent types added to it. This list was based on the unmatched
# file extensions that `find` turned up on `repo.parabola.nu`.
add-file -m755 usr/share/holo/files/10-"$pkgname"/etc/nginx/mime.types.holoscript <<EOF
#!/bin/sh
{
	echo '# -*- Mode: nginx; nginx-indent-level: 4; indent-tabs-mode: nil -*-'
	echo
	sed -e '
/types {/ {
	a\\    application/x-xz                      xz;
	a\\    application/x-gzip                    gz;
	a\\    application/x-bzip2                   bz2;
	a\\    application/x-tar                     tar;
	a\\    application/octet-stream              sig;
	a\\    application/x-bittorrent              torrent;
	a\\

}'
} | awk '\$0==""||!x[\$0]++' | cat -s
EOF

add-file etc/nginx/sites/alias-parabolagnulinux_org.conf <<EOF
# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-
# Redirect everything from *.parabolagnulinux.org to *.parabola.nu

# Top-level domain
server {
	server_name parabolagnulinux.org;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	location / { return 301 https://www.parabola.nu\$request_uri; }
}

# Wildcard sub-domain
server {
	server_name ~^(?<subdomain>[^\\.]*)\\.parabolagnulinux\\.org\$;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	location / { return 301 https://\$subdomain.parabola.nu\$request_uri; }
}
EOF

add-file etc/nginx/sites/meta-unknown-domain.conf <<EOF
# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-

server {
	listen 443 ssl http2 default_server;
	listen [::]:443 ssl http2 default_server;

	return 301 https://www.parabola.nu/404;
}
EOF

add-unit etc/systemd/system/multi-user.target.wants/nginx.service

postamble
}
