. ${BUILDFILE%/*}/common.sh
pkgver=20161112

package() {
preamble
# #### Wiki

depends+=(
	# PHP
	config-mgmt-certbot
	config-mgmt-nginx
	config-mgmt-uwsgi
	uwsgi-plugin-php
	php-apcu-bc
	php-intl
	# External
	mariadb
	imagemagick
	# Deployment
	git
)

add-file -m755 etc/ssl/misc/certbot-get.d/10-wiki <<EOF
#!/bin/bash
echo wiki.{parabola.nu,parabolagnulinux.org}
EOF

add-file etc/nginx/sites/server-wiki_parabola_nu.conf <<EOF
# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-

server {
	server_name wiki.parabola.nu;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	error_log  /var/log/nginx/nginx.http.wiki_parabola_nu.error.log  error;
	access_log /var/log/nginx/nginx.http.wiki_parabola_nu.access.log specific;

	location = /favicon.ico {
		alias /srv/http/\$server_name/skins/ArchLinux/favicon.ico;
	}

	location = /robots.txt {
		alias /srv/http/\$server_name/robots.txt;
	}

	index index.php;
	location /                     { root /var/empty; try_files /bogus @rewrite; }
	location /images/              { }
	location /skins/               { }
	location /resources/           { }
	location /api.php              { root /var/empty; try_files /bogus @php; }
	location /api.php5             { root /var/empty; try_files /bogus @php; }
	location /img_auth.php         { root /var/empty; try_files /bogus @php; }
	location /img_auth.php5        { root /var/empty; try_files /bogus @php; }
	location /index.php            { root /var/empty; try_files /bogus @php; }
	location /index.php5           { root /var/empty; try_files /bogus @php; }
	location /load.php             { root /var/empty; try_files /bogus @php; }
	location /load.php5            { root /var/empty; try_files /bogus @php; }
	location /opensearch_desc.php  { root /var/empty; try_files /bogus @php; }
	location /opensearch_desc.php5 { root /var/empty; try_files /bogus @php; }
	location /profileinfo.php      { root /var/empty; try_files /bogus @php; }
	location /thumb.php            { root /var/empty; try_files /bogus @php; }
	location /thumb.php5           { root /var/empty; try_files /bogus @php; }
	location /thumb_handler.php    { root /var/empty; try_files /bogus @php; }
	location /thumb_handler.php5   { root /var/empty; try_files /bogus @php; }
	location /wiki.phtml           { root /var/empty; try_files /bogus @php; }

	location @rewrite {
		rewrite ^/(.*)\$ /index.php?title=\$1&\$args;
	}

	location @php {
		uwsgi_cache_key \$host\$request_uri;
		uwsgi_cache_valid 5m;
		#uwsgi_cache one;

		include uwsgi_params;
		uwsgi_modifier1 14; # Standard PHP request
		uwsgi_pass unix:/run/uwsgi/wiki.sock;
	}
}
EOF

depends+=(
	cronie
)

add-file etc/cron.spool/wiki <<EOF
0 0 * * * /srv/http/wiki.parabola.nu/FunnyQuestion.conf.php.sh
EOF

add-file etc/uwsgi/wiki.ini <<EOF
[uwsgi]
master = true
processes = 4

uid = %n
gid = http

plugins = php

# Native code is faster than PHP fallback code
php-set = extension=intl.so

# Required for MediaWiki
php-set = extension=iconv.so

php-set = extension=mysqli.so

# Enable OPcache bytecode caching
php-set = zend_extension=opcache.so

# Enable APCu object caching (related to LocalSettings.php:$wgMainCacheType=CACHE_ACCEL)
php-set = extension=apcu.so
php-set = extension=apc.so

# Restrict the files that can be opened:
# - /srv/http/wiki.parabola.nu: duh, the PHP code
# - /usr/bin: to check for diff3, imagemagick, et c.
# - /tmp: eh?
php-set = open_basedir=/srv/http/wiki.parabola.nu:/usr/bin:/tmp
EOF

add-unit etc/systemd/system/sockets.target.wants/uwsgi@wiki.socket

postamble
}
