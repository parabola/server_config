pkgrel=1
arch=(any)
pkgname=$(basename "$BUILDFILE" .PKGBUILD)
case "$pkgname" in
    config-mgmt-*)
	replaces=(${pkgname/-mgmt-/-})
	conflicts=(${pkgname/-mgmt-/-})
	;;
    config-box-*)
	replaces=(${pkgname/-box-/-})
	conflicts=(${pkgname/-box-/-})
	;;
esac

netctl-enable() (
	. "etc/netctl/$1"
	unit=netctl@"$(systemd-escape -- "$1")".service
	install -Dm644 /dev/stdin etc/systemd/system/"${unit}" <<EOF
.include /usr/lib/systemd/system/netctl@.service

[Unit]
Description=${Description}
BindsTo=sys-subsystem-net-devices-${Interface}.device
After=sys-subsystem-net-devices-${Interface}.device
EOF
	add-unit etc/systemd/system/multi-user.target.wants/"$unit"
)

add-unit() (
	install -d "${1%/*}"
	base="$(sed 's/@[^.]*\./@./' <<<"${1##*/}")"
	srcs=(
		"etc/systemd/system/${1##*/}"
		"etc/systemd/system/$base"
		"usr/lib/systemd/system/$base"
	)
	for src in "${srcs[@]}"; do
		if test -f "$src"; then
			break
		fi
		if test -f "/$src"; then
			break
		fi
	done
	ln -s "/$src" "$1"
)

add-file() {
	install -Dm644 /dev/stdin "$@"
}

preamble() {
	cd "$pkgdir"
}

postamble() {
	install -Dm644 "$BUILDFILE" -t "usr/share/doc/config"
	backup=($(find "$pkgdir" -type f -printf '%P\n'))
	local d
	for d in "$pkgdir"/usr/share/holo/*; do
		if [[ -d "$d" ]]; then
			depends+=("holo-${d##*/}")
			install=holo.install
		fi
	done
}
