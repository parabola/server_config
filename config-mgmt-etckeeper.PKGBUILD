. ${BUILDFILE%/*}/common.sh
pkgver=20161230

package() {
preamble
# #### etckeeper

depends+=(etckeeper pristine-etc-keeper)

# [etckeeper][] is configured to use git (the default) to keep track
# of changes in `/etc`. The systemd unit etckeeper.timer is enabled,
# which makes a commit (if necessary) daily.  It will also run before
# and after pacman via libalpm hooks.
#
# [etckeeper]: https://www.parabola.nu/packages/community/any/etckeeper/
add-unit etc/systemd/system/multi-user.target.wants/etckeeper.timer

# In addition to etckeeper keeping track of the current configuration,
# lukeshu's [pristine-etc-keeper][] maintains a branch of what `/etc`
# would be like if we never made any changes from the default
# files. The clean version of etc is available as the clean remote's
# master branch. Doing a git diff pristine/master master should be
# helpful in investigating how things have been configured.
#
# [pristine-etc-keeper]: https://www.parabola.nu/packages/pcr/any/pristine-etc-keeper-git/

# etckeeper is configured to automatically "forget" about files that
# become .gitignore'ed; normally you'd have to manually run
# `git rm --cached` on them.
add-file -m755 etc/etckeeper/commit.d/40git-ignore <<EOF
#!/bin/sh
git ls-files --ignored --exclude-standard -z|xargs -0r git rm --cached --
EOF

# etckeeper has been configured to run the pre-commit hook at exta
# times, to work around [a bug][0].
#
# [0]: https://etckeeper.branchable.com/todo/metadata_changes_don__39__t_cause_a_new_commit/
add-file -m755 etc/etckeeper/unclean.d/20pre-commit <<EOF
#!/bin/sh
# pre-commit hook for etckeeper, to store metadata and do sanity checks
set -e
etckeeper pre-commit -d /etc
EOF
add-file -m755 etc/etckeeper/commit.d/20pre-commit <<EOF
#!/bin/sh
# pre-commit hook for etckeeper, to store metadata and do sanity checks
set -e
etckeeper pre-commit -d /etc
EOF

# It has also been configured to maintain a list of installed packages
# as `/etc/.installed-packages.txt`.
add-file -m755 etc/etckeeper/pre-commit.d/25list-installed <<EOF
#!/bin/sh

etckeeper list-installed > .installed-packages.txt

# stage the file as part of the current commit
if [ "\$VCS" = git ]; then
	# this will do nothing if the file is unchanged.
	git add .installed-packages.txt
fi
EOF

# The etckeeper update-ignore hook is customized to ignore a couple
# more files in `/etc` (and avoid ignoring `/etc/mtab`).  We ignore
# `resolv.conf` because it is managed by [resolvconf][openresolv].
#
# [openresolv]: https://www.parabola.nu/packages/core/any/openresolv/
add-file -m755 etc/etckeeper/update-ignore.d/02custom <<_EOF_
#!/bin/bash
sed -i '/^# end section managed by etckeeper\$/,\$d' .gitignore
cat >> .gitignore <<EOF
# end section managed by etckeeper
!/mtab
/group-
/gshadow-
/passwd-
/shadow-
/.updated
/resolv.conf
EOF
_EOF_

# For files in subdirectories of `/etc`, we use separate `.gitignore`
# files in each directory:
add-file etc/ca-certificates/.gitignore <<EOF
/trust-source/blacklist/
/trust-source/anchors/
/extracted/
EOF
add-file etc/ssl/.gitignore <<EOF
/certs/*.0
/certs/*.1
/certs/*.pem
/certs/java/cacerts
/.ssh/
EOF
add-file etc/udev/.gitignore <<EOF
/hwdb.bin
EOF
add-file etc/pacman.d/.gitignore <<EOF
/gnupg/
EOF
add-file etc/xml/.gitignore <<EOF
/catalog
EOF

postamble
}
