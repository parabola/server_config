. ${BUILDFILE%/*}/common.sh
pkgver=20170320

package() {
preamble
# #### base setup

depends+=(linux-libre-lts grub haveged irqbalance)
conflicts+=(linux-libre)

add-file etc/hostname <<EOF
winston.parabola.nu
EOF

add-file etc/machine-info <<EOF
LOCATION="1984 Hosting Company, Iceland"
EOF

add-file etc/vconsole.conf <<EOF
KEYMAP=us
CONSOLEMAP=8859-1
FONT_MAP=8859-1_to_uni
EOF

add-file etc/locale.conf <<EOF
LANG=en_US.UTF-8
LC_COLLATE=C
EOF

add-unit etc/systemd/system/getty.target.wants/getty@tty1.service
add-unit etc/systemd/system/multi-user.target.wants/haveged.service
add-unit etc/systemd/system/multi-user.target.wants/irqbalance.service
add-unit etc/systemd/system/multi-user.target.wants/remote-fs.target

ln -s ../usr/lib/os-release etc/os-release
install -d etc/ssl/private
ln -s ../../letsencrypt/live/$(cat etc/hostname) etc/ssl/private/myhostname

# ##### timedate

ln -s ../usr/share/zoneinfo/Iceland etc/localtime

# Use timesyncd NTP client daemon.
add-unit etc/systemd/system/sysinit.target.wants/systemd-timesyncd.service

# ##### networking

depends+=(netctl)

# `/etc/udev/rules.d/80-net-setup-link.rules` is a symlink to
# `/dev/null`, which disables new-style [predictable network interface
# names][0] (`enp0s3`? `ens3`?), and causes it to fall back to the
# old-style names (`eth0`).
#
# [0]: https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/
install -d etc/udev/rules.d
ln -s /dev/null etc/udev/rules.d/80-net-setup-link.rules

# The netctl profile eth0-static just has the network information from
# the 1984 VPS control panel.
add-file etc/netctl/eth0-static <<EOF
Description='A basic static ethernet connection'
Interface=eth0
Connection=ethernet
IP=static
Address=('93.95.226.249/25')
Gateway='93.95.226.129'
DNS=('93.95.224.28' '93.95.224.29')
EOF

netctl-enable eth0-static

postamble
}
