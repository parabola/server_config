. ${BUILDFILE%/*}/common.sh
pkgver=20161112

package() {
preamble
# #### Git

depends+=(
	git # for the 'git' user, and obviously git itself.
	cronie
	moreutils # for chronic
)

# Metadata about all of the git repositories, as well as most of the
# setup, lives in the git-meta.git repository. The git user has a
# cron-job to update this repository every hour.
install -d etc/cron.spool
add-file -m600 -o git -g git etc/cron.spool/git <<EOF
MAILTO=maintenance@lists.parabola.nu
PATH=/srv/git/.local/bin:/bin
#min	hour	dom	mon	dow	cmd
0	*	*	*	*	chronic bash -c "update-gitmeta 'Update from cron' 2>&1"
EOF

# ##### transport: git protocol

add-unit etc/systemd/system/sockets.target.wants/git-daemon.socket

# ##### transport: SSH

# Files affected manually:
#
# * `/srv/git/git-shell-commands/`
#
# This is the only method for which push is enabled. Only the git@
# user may push; even though if you have access to git, you probably
# have access to ssh into yourname@winston, you can't push from that
# account. As far as git is concerned, all users are equal (TODO:
# perhaps we should implement some more granular access
# control. Safety rails and all that). Access to git@ is based on
# membership in the "git" group in hackers.git (see
# [#Parabola_hackers](#Parabola_hackers)).
#
# If you need to set up custom git hooks, then ssh in to your user,
# and use `sudo -u git` to do it.

# ##### transport: HTTPS

depends+=(
	cgit
	config-mgmt-certbot
	config-mgmt-nginx
	config-mgmt-uwsgi
	uwsgi-plugin-cgi
)

add-file -m755 etc/ssl/misc/certbot-get.d/10-git <<EOF
#!/bin/bash
echo git.{parabola.nu,parabolagnulinux.org}
EOF

# The git web interface is cgit, which is managed by uWSGI speaking
# uwsgi/modifier1=9, which is the variant of the uwsgi protocol for
# CGI requests.
add-file etc/nginx/sites/server-git_parabola_nu.conf <<EOF
# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-

server {
	server_name git.parabola.nu;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	error_log  /var/log/nginx/nginx.http.git_parabola_nu.error.log  error;
	access_log /var/log/nginx/nginx.http.git_parabola_nu.access.log specific;

	location / {
		try_files \$uri @cgit;
	}

	location @cgit {
		uwsgi_pass unix:/run/uwsgi/git.sock;
		uwsgi_modifier1 9; # Standard CGI request
		include uwsgi_params;
	}
}
EOF

# uwsgi/git.ini sets CGIT_CONFIG=/srv/http/git.parabola.nu/cgitrc.
add-file etc/uwsgi/git.ini <<EOF
[uwsgi]
master = true
processes = 1

uid = %n
gid = http

plugins = cgi
env = CGIT_CONFIG=/srv/http/git.parabola.nu/cgitrc
cgi =             /srv/http/git.parabola.nu/cgit.cgi
EOF

add-unit etc/systemd/system/sockets.target.wants/uwsgi@git.socket

postamble
}
