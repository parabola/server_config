. ${BUILDFILE%/*}/common.sh
pkgver=20170202

package() {
preamble
# #### Host information

depends+=(config-mgmt-nginx)

add-file etc/nginx/sites/server-myhostname.conf <<EOF
# -*- Mode: nginx; nginx-indent-level: 8; indent-tabs-mode: t -*-

server {
	server_name \$hostname;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	error_log  /var/log/nginx/nginx.http.myhostname.error.log  error;
	access_log /var/log/nginx/nginx.http.myhostname.access.log specific;

	root /srv/http/myhostname;
	autoindex on;

	add_header "Access-Control-Allow-Origin" "*";

	location = /sysinfo {
		uwsgi_pass unix:/run/uwsgi/myhostname.sock;
		uwsgi_modifier1 9; # Standard CGI request
		include uwsgi_params;
	}
}
EOF

# Let's have a simple CGI script show server info

depends+=(
	config-mgmt-uwsgi
	uwsgi-plugin-cgi
)

add-file -m755 srv/http/myhostname/sysinfo <<EOF
#!/bin/bash
# Copyright 2016 Luke Shumaker

cmds=(
	'hostnamectl status'
	"systemctl status | sed '1d;/^\\s*CGroup:/,\\\$d'"
	'uptime'
	'free -h'
	"df -Th | sed -n '1p;/^\\/dev/p'"
)

printf '%s\\r\\n' \\
       'Content-Type: text/plain; charset=utf-8' \\
       ''

for cmd in "\${cmds[@]}"; do
	printf '\$ %s\\n' "\$cmd"
	(eval "\$cmd")
	echo
done
EOF

add-file etc/uwsgi/myhostname.ini <<EOF
[uwsgi]
master = true
processes = 1

uid = nobody
gid = http

plugins = cgi
cgi = /srv/http/myhostname/sysinfo
EOF

add-unit etc/systemd/system/sockets.target.wants/uwsgi@myhostname.socket

# Expose the pacman cache so pinned packages can be duplicated
ln -s /var/cache/pacman/pkg srv/http/myhostname/pkg

# Expose collectd via RRD files
ln -s /var/lib/collectd srv/http/myhostname/collectd

depends+=(
	config-base-collectd
	rrdtool
	config-mgmt-pacman-repo-lukeshu # for systemd-timesyncd-wait-git
	systemd-timesyncd-wait-git
)

add-unit etc/systemd/system/multi-user.target.wants/collectd.service

add-file -m644 etc/systemd/system/collectd.service.d/time-sync.conf <<EOF
[Unit]
# Because we write RRD files with timestamps in them, we need to have
# a functional clock.  Otherwise the RRD files will get garbled.
After=time-sync.target
EOF

postamble
}
