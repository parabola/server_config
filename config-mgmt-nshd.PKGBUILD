. ${BUILDFILE%/*}/common.sh
pkgver=20170816

package() {
preamble
# #### Parabola hackers

depends=(parabola-hackers-nshd openssh)

# sshd is configured to force the use of keys (no password-based
# login), and to use [parabola-hackers][] `ssh-list-authorized-keys`
# in addition to checking `~/.ssh/authorized_keys`.
# `ssh-list-authorized-keys` returns the authorized keys from the
# [hackers.git][] checkout in `/var/lib/hackers-git` (the path to the
# checkout is configured in `/etc/parabola-hackers.yml`).
#
# [parabola-hackers]: https://www.parabola.nu/packages/libre/x86_64/parabola-hackers/
# [hackers.git]: https://git.parabola.nu/hackers.git/
add-file -m755 usr/share/holo/files/10-"$pkgname"/etc/ssh/sshd_config.holoscript <<EOF
#!/bin/sh
{
	sed -e '/^#AuthorizedKeysCommand\s/     aAuthorizedKeysCommand /usr/lib/parabola-hackers/ssh-list-authorized-keys' \\
	    -e '/^#AuthorizedKeysCommandUser\s/ aAuthorizedKeysCommandUser nshd' \\
	    -e '/^#PasswordAuthentication\s/    aPasswordAuthentication no'
} | awk '\$0==""||!x[\$0]++'
EOF

# NSS and PAM have been configured to use the ldap modules that are
# part of [nss-pam-ldapd][].
#
# [nss-pam-ldapd]: https://www.parabola.nu/packages/community/x86_64/nss-pam-ldapd/
add-file -m755 usr/share/holo/files/10-"$pkgname"/etc/nsswitch.conf.holoscript <<EOF
#!/bin/sh
sed 's/ ldap//' | sed -r '/^(passwd|group|shadow):/s/(files|compat)/files ldap/'
EOF
add-file -m755 usr/share/holo/files/10-"$pkgname"/etc/pam.d/passwd.holoscript <<EOF
#!/bin/sh
sed -e '/ldap/d' |
sed -e 's/^password	required	pam_unix[.]so/#&/' \\
    -e '\$apassword	required	pam_ldap.so minimum_uid=1000'
EOF

# However, instead of running the normal `nslcd` LDAP client daemon,
# the system has ben configured to run the [parabola-hackers-nshd][]
# `nshd` daemon, which reads user infomation from the same
# `hackers.git` checkout (configured the same way).  This way we dn't
# have to worry about keeping `/etc/passwd` in sync with
# `hackers.git`.
#
# [parabola-hackers-nshd]: https://www.parabola.nu/packages/libre/x86_64/parabola-hackers-nshd/
add-unit etc/systemd/system/sockets.target.wants/nshd.socket
add-unit etc/systemd/system/dbus.service.wants/nshd.service # (temporary [systemd bug workaround][])
#
# [sytemd bug workaround][]: https://projects.parabola.nu/packages/parabola-hackers.git/tree/nshd.service.in#n19

# To this end, PAM has also been configured to create a users home
# directory when they log in if it doesn't already exist.
add-file -m755 usr/share/holo/files/10-"$pkgname"/etc/pam.d/system-login.holoscript <<EOF
#!/bin/sh
sed '/pam_mkhomedir/d'
echo 'session    required   pam_mkhomedir.so     skel=/etc/skel umask=0077'
EOF

# Because `hackers.git` doesn't store any password information, `nshd`
# stores password hashes in `/etc/nshd/shadow`.

postamble
}
